package ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// This is just a sample frame
public class TestUi extends JFrame {

    private JButton uiButQuery = new JButton("Query"), // This is query
            uiButFilter = new JButton("Filter"), // This is filter
            uiButExit = new JButton("Exit"); // This is exit

    private JTextArea uiTxaText = new JTextArea(30, 30); // Text information

    public TestUi() {
        super("Demonstration");

        setSize(400, 400); // Little size here and there

        setLayout(new BorderLayout(10, 10));

        JPanel uiPnl = (JPanel) getContentPane();
        uiPnl.setLayout(new BorderLayout());

        uiPnl.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPanel uiToolBar = new JPanel(new FlowLayout(3));

        uiPnl.add(uiToolBar, BorderLayout.NORTH);
        uiPnl.add(new JScrollPane(uiTxaText), BorderLayout.CENTER);

        uiToolBar.add(uiButQuery);
        uiToolBar.add(uiButFilter);
        uiToolBar.add(uiButExit);

        uiButQuery.addActionListener(e -> {
            JOptionPane.showMessageDialog(TestUi.this, "You press Query", "Query", JOptionPane.ERROR_MESSAGE);
        });

        uiButFilter.addActionListener(e -> {
            // JOptionPane.showMessageDialog(TestUi.this, "You press Filter", "Filter", JOptionPane.ERROR_MESSAGE);

            MyInfoUi uiInfo = new MyInfoUi(TestUi.this);
            uiInfo.setLocationRelativeTo(TestUi.this);
            uiInfo.setVisible(true);
        });

        uiButExit.addActionListener((e) -> {
            System.exit(0); // Exit the system
        });

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                uiTxaText.requestFocus(); // This is only a test
            }
        });

    }

    public static void main(String[] args) {
        TestUi ui = new TestUi();

        ui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        ui.setLocation(100, 100);

        ui.setVisible(true);

    }

}
