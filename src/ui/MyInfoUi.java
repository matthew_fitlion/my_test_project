package ui;

import javax.swing.*;
import java.awt.*;

public final class MyInfoUi extends JDialog {

    private JButton uiButTest = new JButton("Test"); // Button

    private JFrame frame;

    public MyInfoUi(JFrame frame) {
        super(frame, true);

        this.frame = frame;

        getContentPane().setLayout(new BorderLayout());

        add(uiButTest); // fish ball

        uiButTest.addActionListener(e -> {
            JOptionPane.showMessageDialog(frame, "This is just testing" + "What a great memory");
        });

        setSize(100, 100); // Good size here

    }

}
